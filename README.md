# MSigDB 5.0 Gene Sets

A collection of all gene sets from MSigDB 5.0 parsed into an R list object for ease of use with Bioconductor packages.

```r
## MSigDB 5.0 Gene Sets
genesets <- strsplit(readLines('msigdb.v5.0.entrez.gmt'), split = '\t')
genesets <- structure(lapply(genesets, '[', -c(1:2)),
                      names = sapply(genesets, '[', 1))
```

##### Usage:
```r
devtools::install_bitbucket('jeevb/MSigDB5')
library(MSigDB5)
data(genesets)
```
